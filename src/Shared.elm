module Shared exposing
    ( CheckboxInputParams
    , CommonInputParams
    , FieldModel
    , FormModel
    , FormStatus(..)
    , InputKind(..)
    , Msg(..)
    , PasswordInputParams
    , TextInputParams
    , onClick
    )

import Dict exposing (Dict)
import Html
import Html.Events exposing (custom)
import Json.Decode as Decode



-------------------------------------------------------------------------------
--> Main
-------------------------------------------------------------------------------


type Msg
    = InputChanged String String
    | CheckboxChecked String Bool
    | FormSubmitted
    | GotBackendResponse
    | RevealPasswordClicked String Bool


onClick : msg -> Html.Attribute msg
onClick message =
    custom "click"
        (Decode.succeed
            { message = message
            , stopPropagation = False
            , preventDefault = True
            }
        )



-------------------------------------------------------------------------------
--> Field
-------------------------------------------------------------------------------


type InputKind
    = Text
    | Email
    | Password
    | ConfirmPassword
    | Agree


type alias FieldModel =
    { order : Int
    , name : String
    , label : String
    , value : String
    , checked : Bool
    , revealed : Bool
    , error : String
    , kind : InputKind
    , required : Bool
    }



-------------------------------------------------------------------------------
--> Form
-------------------------------------------------------------------------------


type FormStatus
    = Init
    | Validated
    | Error


type alias FormModel =
    { fields : Dict String FieldModel
    , status : FormStatus
    }



-------------------------------------------------------------------------------
-->  Input
-------------------------------------------------------------------------------


type alias TextInputParams a =
    { a
        | order : Int
        , name : String
        , label : String
        , value : String
        , error : String
        , kind : InputKind
        , required : Bool
    }


type alias PasswordInputParams a =
    { a
        | order : Int
        , name : String
        , label : String
        , value : String
        , revealed : Bool
        , error : String
        , kind : InputKind
        , required : Bool
    }


type alias CheckboxInputParams a =
    { a
        | order : Int
        , name : String
        , label : String
        , checked : Bool
        , error : String
        , kind : InputKind
        , required : Bool
    }


type alias CommonInputParams a =
    { a
        | order : Int
        , name : String
        , label : String
        , error : String
        , kind : InputKind
        , required : Bool
    }
