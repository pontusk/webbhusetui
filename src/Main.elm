module Main exposing
    ( Flags
    , Model
    , View
    , main
    )

import Browser
import Dict
import Field
import Form
import Html
    exposing
        ( Attribute
        , Html
        , article
        , button
        , div
        , h1
        , h2
        , li
        , node
        , p
        , section
        , text
        , ul
        )
import Html.Attributes exposing (class)
import Html.Events exposing (onSubmit)
import Input exposing (viewAgreeInput, viewPasswordInput, viewTextInput)
import Process
import Shared exposing (FormModel, FormStatus(..), InputKind(..), Msg(..))
import Task



-------------------------------------------------------------------------------
--> Type stuff
-------------------------------------------------------------------------------


type alias Model =
    { form : FormModel
    , view : View
    }


type View
    = FillForm
    | SendingForm
    | SubmitSuccess


type alias Flags =
    ()



-------------------------------------------------------------------------------
--> Main stuff
-------------------------------------------------------------------------------


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


init : Flags -> ( Model, Cmd Msg )
init _ =
    ( initModel
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-------------------------------------------------------------------------------
--> Model stuff
-------------------------------------------------------------------------------


initModel : Model
initModel =
    { form = initForm
    , view = FillForm
    }


initForm : FormModel
initForm =
    { fields =
        Form.create
            [ Field.create 1
                "firstname"
                "First Name"
                Text
            , Field.create 2
                "lastname"
                "Last Name"
                Text
            , Field.create 3
                "email"
                "Email"
                Email
                |> Field.required True
            , Field.create 4
                "password"
                "Password"
                Password
                |> Field.required True
            , Field.create 5
                "confirmPassword"
                "Confirm Password"
                ConfirmPassword
                |> Field.required True
            , Field.create 6
                "agree"
                "I agree to terms and conditions"
                Agree
                |> Field.required True
            ]
    , status = Init
    }


features : List String
features =
    [ "Lorem ipsum"
    , "Dolor eveniet"
    , "Mollitia omnis sequi obcaecati"
    , "Nobis"
    , "Nam iure sit earum"
    , "Perspiciatis accusamus numquam"
    , "Obcaecati"
    , "Dolor omnis"
    ]



-------------------------------------------------------------------------------
--> Update stuff
-------------------------------------------------------------------------------


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        InputChanged key str ->
            ( { model
                | form =
                    Form.updateField key
                        (Field.value str model.form)
                        model.form
              }
            , Cmd.none
            )

        CheckboxChecked key checked ->
            ( { model
                | form =
                    Form.updateField key
                        (Field.checked checked)
                        model.form
              }
            , Cmd.none
            )

        FormSubmitted ->
            if Form.validate model.form then
                ( { model
                    | view = SendingForm
                    , form = Form.updateStatus Validated model.form
                  }
                , Process.sleep 1000
                    |> Task.andThen (\_ -> Task.succeed GotBackendResponse)
                    |> Task.perform identity
                )

            else
                ( { model | form = Form.updateStatus Error model.form }, Cmd.none )

        GotBackendResponse ->
            ( { model | view = SubmitSuccess }
            , Cmd.none
            )

        RevealPasswordClicked key revealed ->
            ( { model
                | form =
                    Form.updateField key (Field.revealed revealed) model.form
              }
            , Cmd.none
            )



-------------------------------------------------------------------------------
--> View stuff
-------------------------------------------------------------------------------


view : Model -> Html Msg
view model =
    div
        []
        [ node "style" [] [ text css ]
        , viewHero
        , case model.view of
            FillForm ->
                viewForm model.form

            SendingForm ->
                viewLoading model

            SubmitSuccess ->
                viewSuccess model
        ]


viewHero : Html msg
viewHero =
    section [ class "hero" ]
        [ div [ class "hero__text" ]
            [ h1 [] [ text "Create account!" ]
            , p [] [ text "Dolor eveniet mollitia omnis sequi obcaecati. Nobis sit nam iure sit earum. Dolorem natus dolore perspiciatis accusamus numquam maiores lorem!" ]
            ]
        ]


viewSubmitButton : List (Attribute msg) -> List (Html msg) -> FormModel -> Html msg
viewSubmitButton attributes markup form =
    div [ class "submit-button" ]
        [ button attributes markup
        , p [ class "error-text" ]
            [ text
                (case form.status of
                    Init ->
                        ""

                    Validated ->
                        ""

                    Error ->
                        "Please check the form for errors and try again"
                )
            ]
        ]


viewForm : FormModel -> Html Msg
viewForm form =
    section [ class "content" ]
        [ Html.form [ onSubmit FormSubmitted ]
            (form.fields
                |> Dict.toList
                |> List.sortBy (\( _, field ) -> field.order)
                |> List.map
                    (\( key, field ) ->
                        case field.kind of
                            Text ->
                                viewTextInput key field form

                            Email ->
                                viewTextInput key field form

                            Password ->
                                viewPasswordInput key field form

                            ConfirmPassword ->
                                viewPasswordInput key field form

                            Agree ->
                                viewAgreeInput key field form
                    )
                |> (\list ->
                        List.append list
                            [ viewSubmitButton []
                                [ text "Create Account" ]
                                form
                            , p [ class "small-text" ] [ text "*Required fields" ]
                            ]
                   )
            )
        , article []
            [ h2 [] [ text "Lots of features" ]
            , p [] [ text "Get access to our full set of features by registering, including but not limited to:" ]
            , ul
                []
                (List.map (\feature -> li [] [ text feature ]) features)
            ]
        ]


viewLoading : Model -> Html msg
viewLoading _ =
    p [ class "loading-text" ] [ text "Loading ..." ]


viewSuccess : Model -> Html msg
viewSuccess _ =
    h2 [ class "success-text" ] [ text "Thank you for registering!" ]



-------------------------------------------------------------------------------
--> Style stuff
-------------------------------------------------------------------------------


css : String
css =
    """
@import url('https://fonts.googleapis.com/css2?family=Raleway:wght@300;600;900&display=swap');

body {
    font-family: 'Raleway', sans-serif;
    font-size: 16px;
    margin: 0;
}

.hero {
    background-image: url("https://i.picsum.photos/id/665/1600/400.jpg?hmac=_bqybN6OSwmLs5ZvnuUz2GMWU5ZnLaQCAXc5CHsmGYs");
    background-color: black;
    height: 400px;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    align-items: center;
    justify-content: center;
}

.hero__text {
    color: white;
    text-align: center;
    margin: 0 2rem 0 2rem;
    max-width: 72rem;
    padding: 2rem;
    background-color: rgba(0, 0,0,0.2);
    backdrop-filter: blur(2px) saturate(75%);
    border-radius: 10px;
    border: 1px solid white;
}

.hero__text h1 {
    text-transform: uppercase;
}

* > h1:first-child,
* > h2:first-child {
    margin-top: 0;
}

* > p:last-child {
    margin-bottom: 0;
}

.hero__text p {
  font-weight: 600;
}

.content {
    margin: 0 auto;
    padding: 2rem;
    max-width: 72rem;
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(calc(375px - 4rem), 1fr));
    grid-gap: 2rem;

}

.input {
    margin-bottom: 1rem;
    position: relative;
    display: grid;
    grid-template-columns: 1fr auto;
    grid-template-areas: "a b"
                         "c c";
    flex-direction: column;
}

.input__inner-wrapper {
    display: flex;
}

.input label {
    position: absolute;
    top: 0.2rem;
    left: 0.5rem;
    pointer-events: none;
    background-color: white;
    transition: transform 0.2s;
    transform-origin: left center;
}

.input input {
    flex-grow: 1;
    margin: 0;
    height: 1.4rem;
    border: 1px solid black;
    border-radius: 5px;
    padding-left: 0.5rem;
    padding-right: 0.5rem;
    font-family: monospace;
    grid-area: a;
}

.input.input--checkbox {
    justify-content: center;
    align-items: flex-start;
}

.input.input--checkbox input {
    height: calc(1.4rem + 4px);
    width: 1.2rem;
    flex-grow: 1;
}

.input.input--checkbox label {
    left: 1.6rem;
    pointer-events: auto;
}

.input input:focus-visible {
    outline: black solid 1px;
}

.input:not(.input--checkbox) input:focus-visible ~ label,
.input:not(.input--checkbox).input--filled label {
    transition: transform 0.2s;
    transform: translateY(calc(2px - 1rem)) scale(0.7);
    transform-origin: left center;
}

.error-text {
    margin: 0.2rem 0 0 0;
    font-size: 0.6rem;
    color: fireBrick;
    font-weight: 600;
    grid-area: c;
}

.small-text {
    font-size: 0.8rem;
}

.submit-button {
    display: flex;
    flex-direction: column;
    align-items: center;
}

button {
  border: 4px double black;
  padding : 0.5rem 1rem;
  border-radius: 5px;
  margin-bottom: 0.5rem;
  font-family: 'Raleway', sans-serif;
  font-weight: 600;
}

button:focus {
    outline: black solid 1px;
}

button:active {
    background-color: black;
    color: white;
    border-color: white;
}

.password-reveal-btn {
    padding: 0 0.5rem;
    height: calc(1.4rem + 4px);
    margin: 0 0 0 0.4rem;
    min-width: 4rem;
    border: none;
    grid-area: b;
}

article {
    color: black;
    border-radius: 10px;
    padding: 2rem;
    border: 2px dotted black;
}

article h2 {
    text-align: center;
    text-decoration: underline;
}

article p {
  font-weight: 600;
}

.loading-text {
    text-align: center;
    margin-top: 4rem;
}

.success-text {
    text-align: center;
    text-decoration: underline;
    margin-top: 4rem;
}

"""
