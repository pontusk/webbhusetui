module Field exposing
    ( checked
    , create
    , required
    , revealed
    , validate
    , value
    )

import Dict
import Regex
import Shared
    exposing
        ( CheckboxInputParams
        , FieldModel
        , FormModel
        , InputKind(..)
        , PasswordInputParams
        , TextInputParams
        )



-------------------------------------------------------------------------------
--> Model stuff
-------------------------------------------------------------------------------


initField : FieldModel
initField =
    { order = 0
    , name = ""
    , label = ""
    , value = ""
    , checked = False
    , revealed = True
    , error = ""
    , kind = Text
    , required = False
    }



-------------------------------------------------------------------------------
--> Main stuff
-------------------------------------------------------------------------------


{-| Create a field record inside a form.

    Form.create
        [ Field.create 1 "field-name" "Field Label" Text
        ]

The arguments are the parameters that always need to be supplied to initialize
a new field. Additional parameters can be customized at form creation (as well
as updated later) using the field update functions.

    Form.create
        [ Field.create 1 "field-name" "Field Label" Text
            |> Field.required True
        ]
-}
create : Int -> String -> String -> InputKind -> FieldModel
create order name label kind =
    { initField
        | order = order
        , name = name
        , label = label
        , kind = kind
        , revealed = kind /= Password && kind /= ConfirmPassword
    }



-------------------------------------------------------------------------------
--> Update stuff
-------------------------------------------------------------------------------


value : String -> FormModel -> FieldModel -> FieldModel
value str form field =
    { field
        | value = str
        , error = validate { field | value = str } form |> Tuple.second
    }


revealed : Bool -> FieldModel -> FieldModel
revealed bool field =
    { field
        | revealed = bool
    }


checked : Bool -> FieldModel -> FieldModel
checked bool field =
    { field | checked = bool }


required : Bool -> FieldModel -> FieldModel
required bool field =
    { field | required = bool }



-------------------------------------------------------------------------------
--> Validation stuff
-------------------------------------------------------------------------------


{-| Validate a field.

    Field.validate field form
-}
validate : FieldModel -> FormModel -> ( Bool, String )
validate field_ =
    let
        validateText : TextInputParams a -> FormModel -> ( Bool, String )
        validateText field _ =
            if field.value == "" && field.required then
                ( False, "" )

            else
                ( True, "" )

        validateEmail : TextInputParams a -> FormModel -> ( Bool, String )
        validateEmail field _ =
            if field.value == "" then
                ( True, "" )

            else if not <| String.contains "@" field.value then
                ( False, "Invalid email" )

            else if String.endsWith "example.com" field.value then
                ( False, "Email already registered" )

            else if field.value == "" && field.required then
                ( False, "" )

            else
                ( True, "" )

        validatePassword : PasswordInputParams a -> FormModel -> ( Bool, String )
        validatePassword field _ =
            let
                checkPasswordRegex : String -> Bool
                checkPasswordRegex str =
                    str
                        |> Regex.contains
                            (Maybe.withDefault Regex.never <|
                                Regex.fromString "(?=.*[A-Z])(?=.*\\d)(?=.*[!@#\\$%\\^&\\*])"
                            )
            in
            if field.value == "" then
                ( True, "" )

            else if not <| checkPasswordRegex field.value then
                ( False, "Password must have 1 capital, 1 digit and 1 special character" )

            else if String.length field.value < 6 then
                ( False, "Password must be at least 6 chars" )

            else if field.value == "" && field.required then
                ( False, "" )

            else
                ( True, "" )

        validateConfirmPassword : PasswordInputParams a -> FormModel -> ( Bool, String )
        validateConfirmPassword field form =
            let
                password : String
                password =
                    case Dict.get "password" form.fields of
                        Just f ->
                            f.value

                        Nothing ->
                            ""
            in
            if password /= field.value then
                ( False, "Passwords do not match" )

            else if field.value == "" && field.required then
                ( False, "" )

            else
                ( True, "" )

        validateAgree : CheckboxInputParams a -> FormModel -> ( Bool, String )
        validateAgree field _ =
            if not field.checked && field.required then
                ( False, "" )

            else
                ( True, "" )
    in
    case field_.kind of
        Text ->
            validateText field_

        Email ->
            validateEmail field_

        Password ->
            validatePassword field_

        ConfirmPassword ->
            validateConfirmPassword field_

        Agree ->
            validateAgree field_
