module Input exposing (viewAgreeInput, viewPasswordInput, viewTextInput)

import Html
    exposing
        ( Attribute
        , Html
        , button
        , div
        , input
        , label
        , p
        , text
        )
import Html.Attributes exposing (class, classList, for, id, name, type_)
import Html.Events exposing (onCheck, onInput)
import Shared
    exposing
        ( CheckboxInputParams
        , CommonInputParams
        , FormModel
        , FormStatus(..)
        , Msg(..)
        , PasswordInputParams
        , TextInputParams
        , onClick
        )


viewRevealButton : String -> Bool -> Html Msg
viewRevealButton key revealed =
    button
        [ class "password-reveal-btn"
        , onClick
            (RevealPasswordClicked key (not revealed))
        ]
        [ text
            (if revealed then
                "hide"

             else
                "show"
            )
        ]


viewErrorText : String -> Bool -> FormStatus -> CommonInputParams a -> Html Msg
viewErrorText error condition status field =
    let
        errorText : String
        errorText =
            if error /= "" then
                error

            else if status == Error && condition && field.required then
                "This field is required"

            else
                ""
    in
    p [ class "error-text" ]
        [ text errorText ]


viewTextInput : String -> TextInputParams a -> FormModel -> Html Msg
viewTextInput key field form =
    viewInput key
        field
        form
        (field.value == "")
        "text"
        [ onInput (InputChanged key) ]
        []


viewPasswordInput : String -> PasswordInputParams a -> FormModel -> Html Msg
viewPasswordInput key ({ value, revealed } as field) form =
    viewInput key
        field
        form
        (value == "")
        (if revealed then
            "text"

         else
            "password"
        )
        [ onInput (InputChanged key) ]
        [ viewRevealButton key revealed ]


viewAgreeInput : String -> CheckboxInputParams a -> FormModel -> Html Msg
viewAgreeInput key field form =
    viewInput key
        field
        form
        (not field.checked)
        "checkbox"
        [ onCheck (CheckboxChecked key), type_ "checkbox" ]
        []


viewInput : String -> CommonInputParams a -> FormModel -> Bool -> String -> List (Attribute Msg) -> List (Html Msg) -> Html Msg
viewInput key field form errorCondition inputType attributes markup =
    div
        [ class ("input input--" ++ inputType)
        , classList [ ( "input--filled", not errorCondition ) ]
        ]
        ([ input
            ([ name key
             , id key
             , type_ inputType
             ]
                ++ attributes
            )
            []
         , label [ for key ]
            [ text
                (field.label
                    ++ (if field.required then
                            "*"

                        else
                            ""
                       )
                )
            ]
         , viewErrorText field.error errorCondition form.status field
         ]
            ++ markup
        )
