module Form exposing
    ( create
    , updateField
    , updateStatus
    , validate
    )

import Dict exposing (Dict)
import Field
import Shared exposing (FieldModel, FormModel, FormStatus)



-------------------------------------------------------------------------------
--> Main stuff
-------------------------------------------------------------------------------


{-| Create a form from a list of fields created using the Field module.

    Form.create
        [ Field.create 1 "field-name" "Field Label" Text
        ]

-}
create : List FieldModel -> Dict String FieldModel
create fields =
    fields
        |> List.map (\field -> ( field.name, field ))
        |> Dict.fromList



-------------------------------------------------------------------------------
--> Update stuff
-------------------------------------------------------------------------------


{-| Update a field in the form. The updater argument should be one of the field 
update functions from the Field module.

    { model
      | form =
          Form.updateField key
              (Field.checked checked)
              form
    }
-}
updateField : String -> (FieldModel -> FieldModel) -> FormModel -> FormModel
updateField key updater form =
    { form | fields = Dict.update key (Maybe.map updater) form.fields }


updateStatus : FormStatus -> FormModel -> FormModel
updateStatus status form =
    { form | status = status }



-------------------------------------------------------------------------------
--> Validation stuff
-------------------------------------------------------------------------------


{-| Validate the form in it's entirety.

    Form.validate form
-}
validate : FormModel -> Bool
validate form =
    form.fields
        |> Dict.toList
        |> List.all
            (\( _, field ) ->
                Tuple.first (Field.validate field form)
            )
