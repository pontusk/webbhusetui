# WebbhusetUI



## TODO

- [x] Implement a visually pleasing design without sacrificing usability
- [x] Make it responsive and usable on all screen sizes
- [x] Implement the hero in a responsive fashion
        - Feel free to adjust the HTML DOM as needed
        - The image should always take the full width of the screen (or a container)
        - The image should always be 400px in height
        - The person in the photo should always be visible regardless of screen width
        - The title and text should be vertically and horizontally aligned center and never overflow the container
- [x] Fix Accessibility
- [x] Validate that password contains at least 1 capital, 1 digit and 1 special character.
- [x] confirmPassword and password fields must match. If not, form should not be submittable.
- [x] Make a show/hide button to display password in cleartext at will.
- [x] Form should not submit unless all fields are valid.
- [x] Form should not submit unless terms and condition checkbox is "checked"
- [x] Add feedback that the app is loading when submit is clicked.
- [x] Refactor code to make it more maintainable and easier to understand.
